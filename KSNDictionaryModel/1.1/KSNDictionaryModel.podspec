#
# Be sure to run `pod lib lint KSNDictionaryModel.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "KSNDictionaryModel"
  s.version          = "1.1"
  s.summary          = "Wrapper around NSDictionary."

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!  
  s.description      = "KSNDictionaryModel is a NSDictionary wrapper that simplifies and formalizes usage of the NSDictionary as a data model in your app. Please see Tests for more details."

  s.homepage         = "https://bitbucket.org/atomicip/ksndictionarymodel"
  # s.screenshots     = "www.example.com/screenshots_1", "www.example.com/screenshots_2"
  s.license          = 'MIT'
  s.author           = { "Sergey Kovalenko" => "papuly@gmail.com" }
  s.source           = { :git => "https://bitbucket.org/atomicip/ksndictionarymodel.git", :branch => "master"}
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source_files = 'Pod/Classes/**/*'
# s.resource_bundles = {
#    'KSNDictionaryModel' => ['Pod/Assets/*.png']
#  }

  s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
